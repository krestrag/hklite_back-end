CREATE DATABASE HKtest;

CREATE TABLE role(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255)
);

CREATE TABLE users(
    id SERIAL PRIMARY KEY,
    login VARCHAR(255),
    pass VARCHAR(255),
    email VARCHAR(255),
    role INTEGER,
    FOREIGN KEY (role) REFERENCES role(id)
);

CREATE TABLE category(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    value INTEGER
);