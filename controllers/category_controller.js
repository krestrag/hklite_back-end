const db = require('../db');

class CategoryController{
    async createTag(req,res){
        const {name, value} = req.body;
        const newCategory = await db.module.query('INSERT INTO category (name, value) values ($1, $2) RETURNING *', [name, value]);
        res.json(newCategory.rows[0]);
    }
    async getTags(req,res){
        const category = await db.module.query('SELECT * from category');
        res.json(category.rows);
    }
    async getTag(req,res){
        const id = req.params.id;
        const category = await db.module.query('SELECT * from category where id = $1',[id]);
        res.json(category.rows);
    }
    async updateTag(req,res){
        const {id, name, value} = req.body;
        const updatedCategory = await db.module.query('UPDATE category set name = $1, value = $2 where id = $3 RETURNING *', [name, value, id]);
        res.json(updatedCategory.rows);
    }
    async deleteTag(req,res){
        const id = req.params.id;
        const deletedCategory = await db.module.query('DELETE FROM category where id = $1 RETURNING *',[id])
        res.json(deletedCategory.rows);
    }
}

module.exports = new CategoryController()