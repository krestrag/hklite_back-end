const Router = require('express');
const cat_router = new Router();
const CategoryController = require("../controllers/category_controller")

cat_router.post("/cat", CategoryController.createTag)
cat_router.get("/cat", CategoryController.getTags)
cat_router.get("/cat/:id", CategoryController.getTag)
cat_router.put("/cat", CategoryController.updateTag)
cat_router.delete("/cat/:id", CategoryController.deleteTag)

module.exports = cat_router