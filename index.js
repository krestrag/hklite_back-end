const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const userRouter = require('./routes/user_routes')
const catRouter = require('./routes/category_routes')
const db = require('./db');

const PORT = process.env.PORT || 3000;

const app = express();

app.use(express.json());
app.use(cors());

app.use('/api', userRouter);
app.use('/api', catRouter);
app.use(bodyParser.urlencoded({ extended: false }));

db.module.connect()
  .then(console.log(`База подключена`)); 

app.listen(PORT, () => {
    console.log(`Сервер запущен. Порт: ${PORT}.`);
  });